import { NgForm } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UsuarioService } from '../services/service.index';
import { Usuario } from '../models/usuario.model';

declare function init_plugins();
declare const gapi: any;


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email: string;
  identificador: string;

  recuerdame: boolean = false;

  type : string;
  opcion : string;

 public ocult: string = 'oculto';


  auth2: any;

  constructor(
    public router: Router,
    public _usuarioService: UsuarioService,
    private _route: ActivatedRoute
  ) { }

  ngOnInit() {

    
    init_plugins();
  
    this.opcion = this._route.snapshot.paramMap.get('type');

    console.log(this._route.snapshot.paramMap.get('type'));

    


    //this.googleInit();

    /*this.email = localStorage.getItem('email') || '';
    if ( this.email.length > 1 ) {
      this.recuerdame = true;
    }*/

  }

  asignarIdentificador(type) {
    switch (type) {
      case '1': this.type ="Ruc";
      break;
   
      case '2': this.type ="Dominio";
      break;

      case '3': this.type ="Correo Electronico";
      break;
   
      default:  this.type ="RUC";
   }

   return this.type;
  }

  googleInit() {

    gapi.load('auth2', () => {

      this.auth2 = gapi.auth2.init({
        client_id: '442737206823-dilej5tevnrv61sovd7bocf5qeafmjs3.apps.googleusercontent.com',
        cookiepolicy: 'single_host_origin',
        scope: 'profile email'
      });

      this.attachSignin( document.getElementById('btnGoogle') );

    });

  }

  attachSignin( element ) {

    this.auth2.attachClickHandler( element, {}, (googleUser) => {

      // let profile = googleUser.getBasicProfile();
      let token = googleUser.getAuthResponse().id_token;

      this._usuarioService.loginGoogle( token )
              .subscribe( () => window.location.href = '#/dashboard'  );

    });

  }


  ingresar( forma: NgForm) {

    if ( forma.invalid ) {
      return;
    }

    let usuario;

  
       usuario = {
        identificador : forma.value.identificador,
        codigo_usuario : forma.value.codigo_usuario,
        clave_usuario : forma.value.password,
        type : this.opcion
       };

    this._usuarioService.login( usuario , forma.value.recuerdame)
                  .subscribe( resp => {
                    this.router.navigate(['/dashboard']); },
                    err => {
                      console.log(err.error);
                    });


  }

  mostrarModalr() {
    this.ocult = '';
  }

  ocultarModal(event) {
    this.ocult = event.valor;
  }

}
