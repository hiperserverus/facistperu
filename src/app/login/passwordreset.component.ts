import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UsuarioService } from '../services/service.index';

@Component({
  selector: 'app-passwordreset',
  templateUrl: './passwordreset.component.html'
})
export class PasswordresetComponent implements OnInit {

 
  @Input('oculto') oculto: string;

  @Output() nuevoValorOculto = new EventEmitter;

  public email: string;

  constructor(public _usuarioService: UsuarioService) { }

  ngOnInit() {
    
  }

  enviarValor(event) {
    this.oculto = 'oculto';
    this.nuevoValorOculto.emit({valor: this.oculto});
  }

  restablecer() {

    this._usuarioService.enviarCorreorec(this.email)
          .subscribe(resp =>{
            this.oculto = 'oculto';
    }, err => {
       console.log(err.error);
    })
  }

}
