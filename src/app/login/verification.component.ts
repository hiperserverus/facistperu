import { Component, OnInit, ɵConsole } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UsuarioService } from '../services/service.index';
import { Router, ActivatedRoute } from '@angular/router';

declare function init_plugins();

@Component({
  selector: 'app-verification',
  templateUrl: './verification.component.html',
  styleUrls: ['./login.component.css']

})
export class VerificationComponent implements OnInit {

  forma: FormGroup;
  public tokenURL: string;
  private usuarioDecodificado: any;
  public accion: string;

  constructor(public _usuarioService: UsuarioService,
              public router: Router,
              private _route: ActivatedRoute) { 

                this.forma = new FormGroup({
                  nombre: new FormControl( null , Validators.required),
                  apellido: new FormControl( null , Validators.required),
                  correo: new FormControl( null , [Validators.required, Validators.email] ),
                  empresa: new FormControl( null),
                  dominio: new FormControl( null ),
                  telefono: new FormControl( null  ),
                  password: new FormControl(null, Validators.required),
                  password2: new FormControl(null, Validators.required)
            
                }, { validators: this.sonIguales('password', 'password2')});

              }
              

  ngOnInit() {
    init_plugins();

    this.tokenURL = this._route.snapshot.paramMap.get('token');
    console.log(this.tokenURL);

    this.activateU();


  }

  sonIguales( campo1: string, campo2: string ) {

    return ( group: FormGroup ) => {

      let pass1 = group.controls[campo1].value;
      let pass2 = group.controls[campo2].value;

      if ( pass1 === pass2 ) {
        return null;
      }

      return {
        sonIguales: true
      };

    };

  }

  activateU(){



    this._usuarioService.decodificarUsuario(this.tokenURL)
                        .subscribe( resp => {

                            console.log(resp);
                            this.usuarioDecodificado = resp;

                            this.forma = new FormGroup({
                              nombre: new FormControl( this.usuarioDecodificado.c_nombre_usuario , Validators.required),
                              apellido: new FormControl( this.usuarioDecodificado.c_apellido_usuario , Validators.required),
                              correo: new FormControl( this.usuarioDecodificado.c_correo , [Validators.required, Validators.email] ),
                              empresa: new FormControl( this.usuarioDecodificado.empresa ),
                              dominio: new FormControl( this.usuarioDecodificado.c_dominio ),
                              telefono: new FormControl( this.usuarioDecodificado.c_telefono ),
                              password: new FormControl(null, Validators.required),
                              password2: new FormControl(null, Validators.required)
                        
                            });

                            this.accion = this.usuarioDecodificado.accion;
                        }, err => {
                    
                          if (err) {
                          
                            this.router.navigate(['/error']);
                          }
                         
                        });

                      
  }

  activarUsuario() {

    if ( this.forma.invalid ) {
      return;
    }

    let usuarioActivation;

  
    usuarioActivation = {
      id: this.usuarioDecodificado.c_usuario_id,
     nombre : this.forma.value.nombre,
     apellido : this.forma.value.apellido,
     correo : this.forma.value.correo,
     empresa : this.forma.value.empresa,
     dominio : this.forma.value.dominio,
     telefono : this.forma.value.telefono,
     password : this.forma.value.password,
     token: this.tokenURL
    }

  

    this._usuarioService.activarUsuario( usuarioActivation, this.accion )
              .subscribe( resp => {
                console.log(resp);
                this.router.navigate(['/login']);
              }, err => {
                console.log(err.error)
              });


  }


}
